package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var (
	webSocketUpgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     checkOrigin,
	}
)

type Manager struct {
	clients  ClientList
	mu       sync.RWMutex
	handlers map[string]EventHandler
	otps     RetentionMap
}

func NewManager(ctx context.Context) *Manager {
	m := &Manager{
		clients:  make(ClientList),
		handlers: make(map[string]EventHandler),
		otps:     NewRetentionMap(ctx, 5*time.Second),
		mu:       sync.RWMutex{},
	}
	m.setupEventHandlers()

	return m
}

func (m *Manager) serveWS(w http.ResponseWriter, r *http.Request) {
	otp := r.URL.Query().Get("otp")
	if otp == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if !m.otps.VerifyOTP(otp) {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	log.Println("new connection")

	// recieve client req, and upgrade the connection
	conn, err := webSocketUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := NewClient(conn, m)

	m.addClient(client)

	// start clients processes
	go client.readMessages()
	go client.writeMessages()

}

func (m *Manager) loginHandler(w http.ResponseWriter, r *http.Request) {
	type UserLoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var req UserLoginRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// implement login system
	if req.Username == "artem" && req.Password == "123" {
		type Response struct {
			OTP string `json:"otp"`
		}

		otp := m.otps.NewOTP()
		resp := Response{
			OTP: otp.Key,
		}

		data, err := json.Marshal(resp)
		if err != nil {
			log.Println(err)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}

	w.WriteHeader(http.StatusUnauthorized)
}

func (m *Manager) setupEventHandlers() {
	m.handlers[EventSendMessage] = SendMessage
}

func (m *Manager) routeEvent(event Event, c *Client) error {
	if handler, ok := m.handlers[event.Type]; ok {
		if err := handler(event, c); err != nil {
			return err
		}
	} else {
		return errors.New("unknown event type")
	}

	return nil
}

func (m *Manager) addClient(client *Client) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.clients[client] = struct{}{}
}

func (m *Manager) removeClient(client *Client) {
	m.mu.Lock()
	defer m.mu.Unlock()

	if _, ok := m.clients[client]; ok {
		client.conn.Close()
		delete(m.clients, client)
	}

}

func SendMessage(event Event, c *Client) error {
	var chatEvent SendMessageEvent

	chatEvent.Message = string(event.Payload)

	log.Println("unmarshalled 1", chatEvent.From, chatEvent.Message)

	var broadMesssage NewMessageEvent

	broadMesssage.Sent = time.Now()
	broadMesssage.From = chatEvent.From
	broadMesssage.Message = chatEvent.Message

	log.Println("unmarshalled 2", broadMesssage.From, broadMesssage.Message, broadMesssage.Sent)

	data, err := json.Marshal(broadMesssage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message")
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventNewMessage,
	}

	// send message to all users
	for client := range c.manager.clients {
		client.egress <- outgoingEvent
	}

	return nil
}

func checkOrigin(r *http.Request) bool {
	origin := r.Header.Get("Origin")

	switch origin {
	case "http://localhost:8080":
		return true
	default:
		return false
	}

}
