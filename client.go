package main

import (
	"encoding/json"
	"log"
    "time"

	"github.com/gorilla/websocket"
)

var (
    pongWait = 10 * time.Second
    pingInterval = (pongWait * 9) / 10
)

type ClientList map[*Client]struct{}

type Client struct {
	conn    *websocket.Conn
	manager *Manager

	// egrees is used to avoid concurrent writes on the websocket connection
	egress chan Event
}

func NewClient(conn *websocket.Conn, manager *Manager) *Client {
	return &Client{
		conn:    conn,
		manager: manager,
		egress:  make(chan Event),
	}
}

// read from frontend
func (c *Client) readMessages() {
	defer func() {
		// cleanup connection
		c.manager.removeClient(c)
	}()

    // set timeline for how long to wait for the pong message
    if err := c.conn.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
        log.Println(err)
        return
    }

    // jumbo frames, set limit for the message
    // if the message is greater than the limit, websocket connection will be closed.
    c.conn.SetReadLimit(512)

    c.conn.SetPongHandler(c.pongHandler)

	for {
		_, payload, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Println("error reading message", err)
			}
			break
		}

		var request Event
		if err := json.Unmarshal(payload, &request); err != nil {
			log.Println("failed to unmarshal json")
		}

		if err := c.manager.routeEvent(request, c); err != nil {
			log.Println("error handling message", err)
		}

		log.Println("recieved from frontend", string(payload))
	}
}

func (c *Client) writeMessages() {
	defer func() {
		c.manager.removeClient(c)
	}()

    // add ticker to send  heartbeat to the client. 
    // browsers handle websocket ping messages(respond with pong) automatically
    ticker := time.NewTicker(pingInterval)

	for {
		select {
		case message, ok := <-c.egress:
			if !ok {
				if err := c.conn.WriteMessage(websocket.CloseMessage, nil); err != nil {
					log.Println("connection closed")
				}
				return
			}

			data, err := json.Marshal(message)
			if err != nil {
				log.Println(err)
				return
			}

			// write message back to frontend
			if err := c.conn.WriteMessage(websocket.TextMessage, data); err != nil {
				log.Println("failed to send message", err)
			}

			log.Println("message sent", "hi from server "+string(data))

        case <-ticker.C:
            log.Println("ping from server to frontend!")

            // send ping to clinet
            if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
                log.Println("ping err happened", err)
                return
            }
		}
	}

}

func (c *Client) pongHandler(pongMsg string) error {
    log.Println("pong from frontend to servere!")
    // we always need to reset the timer, so the connection does not die.
    return c.conn.SetReadDeadline(time.Now().Add(pongWait))
}
